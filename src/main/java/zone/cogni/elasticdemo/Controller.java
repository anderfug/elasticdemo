package zone.cogni.elasticdemo;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import static io.vavr.API.Try;
import static org.elasticsearch.common.xcontent.ToXContent.EMPTY_PARAMS;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@org.springframework.stereotype.Controller
public class Controller {

  private final ElasticRepository elasticRepository;

  public Controller(ElasticRepository elasticRepository) {
    this.elasticRepository = elasticRepository;
  }

  @RequestMapping(value = "/simple-search", method = GET, produces = "application/json")
  public ResponseEntity<?> simpleSearch() {

    SearchResponse response = elasticRepository.getTransportClient()
        .prepareSearch("elasticdemo")
        .setTypes("skill")
        .get();

    return new ResponseEntity<Object>(convertToJson(response), HttpStatus.OK);
  }

  @RequestMapping(value = "/match-query", method = GET, produces = "application/json")
  public ResponseEntity<?> matchQuery_(String field, String value) {

    SearchResponse response = elasticRepository.getTransportClient()
        .prepareSearch("elasticdemo")
        .setTypes("skill")
        .setQuery(QueryBuilders.matchQuery(field, value).fuzziness("AUTO"))
        .get();

    return new ResponseEntity<Object>(convertToJson(response), HttpStatus.OK);
  }

  @RequestMapping(value = "/fuzzy-match-query", method = GET, produces = "application/json")
  public ResponseEntity<?> fuzzyMatchQuery(String field, String value) {

    SearchResponse response = elasticRepository.getTransportClient()
        .prepareSearch("elasticdemo")
        .setTypes("skill")
        .setQuery(QueryBuilders.matchQuery(field, value).fuzziness("AUTO"))
        .get();

    return new ResponseEntity<Object>(convertToJson(response), HttpStatus.OK);
  }

  @RequestMapping(value = "/terms-bucket", method = GET, produces = "application/json")
  public ResponseEntity<?> termsBucket(String field, String aggregationName) {

    SearchResponse response = elasticRepository.getTransportClient()
        .prepareSearch("elasticdemo")
        .setTypes("skill")
        .setSize(0)
        .addAggregation(AggregationBuilders.terms(aggregationName).field(field))
        .get();

    return new ResponseEntity<Object>(convertToJson(response), HttpStatus.OK);
  }

  @RequestMapping(value = "/average-title-length", method = GET, produces = "application/json")
  public ResponseEntity<?> averageTitleLength() {

    SearchResponse response = elasticRepository.getTransportClient()
        .prepareSearch("elasticdemo")
        .setTypes("skill")
        .setSize(0)
        .addAggregation(AggregationBuilders.avg("averageTitleLength")
            .script(new Script("doc['title.keyword'].getValue().length()")))
        .get();

    return new ResponseEntity<Object>(convertToJson(response), HttpStatus.OK);
  }



  private String convertToJson (SearchResponse response)  {
    return Try(() -> response
        .toXContent(XContentFactory.contentBuilder(XContentType.JSON), EMPTY_PARAMS)
        .prettyPrint()
        .string())
        .get();
  }

}
