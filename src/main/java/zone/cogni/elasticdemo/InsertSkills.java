package zone.cogni.elasticdemo;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import io.vavr.control.Try;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InsertSkills implements ApplicationRunner {

  private static final Logger log = LoggerFactory.getLogger(InsertSkills.class);

  private final ElasticRepository elasticRepository;

  public InsertSkills(ElasticRepository elasticRepository) {
    this.elasticRepository = elasticRepository;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {

    log.info("Setting up elastic client ...");
    elasticRepository.prepareClient();

    log.info("Preparing index with mapping ...");
    elasticRepository.prepareIndex("elasticdemo",
        ImmutableMap.of("skill", getJson("skillMapping.json").toString()));

    log.info("Indexing skills ...");
    Map<String, JsonNode> skills = new HashMap<>();

    getJson("skills.json")
        .get("_embedded")
        .get("results")
        .forEach(skill -> skills.put(skill.get("uri").asText(), skill));

    bulkInsert("elasticdemo", "skill", skills);

  }

  private JsonNode getJson (String url) {
    final ObjectMapper objectMapper = new ObjectMapper();
    return Try.of(() -> objectMapper.readTree(new ClassPathResource(url).getFile())).get();
  }

  private void bulkInsert (String index, String type, Map<String, JsonNode> data) {
    TransportClient client = elasticRepository.getTransportClient();

    boolean noChange = client.prepareSearch(index).setTypes(type).setFetchSource(false).get()
        .getHits().totalHits == data.size();
    if (noChange) return;

    BulkRequestBuilder builder = client.prepareBulk();

    data.forEach((id, json) -> {
      builder.add(client.prepareIndex(index, type, id).setSource(json.toString(), XContentType.JSON));
    });

    builder.get();
  }

}
