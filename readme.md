#ElasticSearch

#### Installation

1. Download elastic https://www.elastic.co/downloads/elasticsearch
2. set cluster name in {elasticfolder}/config/elasticsearch.yml:
```
cluster.name: elasticdemo
```
(if you want to use a different name you'll have to set that name in the application.yml file)

####API reference:
https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html

####Summary
* a database
* a search engine
* released 2010
* current version is 5.5
* top 10 in popularity: https://db-engines.com/en/ranking
* based on Lucene, the indexing library
* schemaless
* native Json
* written in java
* open source - maintained by the elastic company
* some features are not free - security and some analysis tools

designed for:

* text search
* dynamic, near real time data.
* dynamic scalability - fully distributed

#### Concepts
* document - an individual data entry
* types - container for a type of document
* mapping - allows explicit definition of a document type


* index - A data location
* shards - contains part of the index data
* nodes - a server, a running elasticsearch instance
* cluster - a collection of nodes
	
#### APIs

1. REST api
    * HTTP
    * Json
    * default port 9200
2. Java API
    * Special TCP elasticsearch protocol
    * Native java client library
    * default port 9300
    
#### ELK stack

* Elastic
* Logstash - event and logs managing
* Kibana - data vizualisation